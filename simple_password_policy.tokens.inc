<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function simple_password_policy_token_info() {
  $types['password_policy'] = [
    'name' => t('Password policy'),
    'description' => t('Tokens related to the password policy.'),
    'needs-data' => 'user'
  ];

  $password_policy['expire_period'] = [
    'name' => t('Expire period'),
    'description' => t("The period after which the last password will expire."),
  ];

  return [
    'types' => $types,
    'tokens' => ['password_policy' => $password_policy],
  ];
}

/**
 * Implements hook_tokens().
 */
function simple_password_policy_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'password_policy') {
    /** @var \Drupal\simple_password_policy\PasswordPolicyInterface $password_policy */
    $password_policy = \Drupal::service('simple_password_policy');

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'expire_period':
          /** @var \Drupal\user\UserInterface $user */
          $user = $data['user'];
          $langcode = $user->getPreferredLangcode();
          $expire_time = $password_policy->getExpireTime($user);
          $replacements[$original] = time_ago($expire_time, "@output", "", $langcode);
          break;
      }
    }
  }

  return $replacements;
}
