<?php

namespace Drupal\simple_password_policy\Password;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Password\DefaultPasswordGenerator;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\simple_password_policy\PasswordPolicyInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Alter the DefaultPasswordGenerator class and generates the password.
 *
 * Generates the password according to the configuration of the password policy.
 */
class PasswordGenerator extends DefaultPasswordGenerator {

  /**
   * The original Password Generator service.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected $originalService;

  /**
   * The password policy service.
   *
   * @var Drupal\simple_password_policy\PasswordPolicyInterface
   */
  protected $policy;

  /**
   * The currently logged in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Initializes the instances to the class variables.
   *
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $original_service
   *   The original Password Generator service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory service.
   * @param \Drupal\simple_password_policy\PasswordPolicyInterface $config
   *   The password policy service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(PasswordGeneratorInterface $original_service, ConfigFactoryInterface $config, PasswordPolicyInterface $policy, AccountProxyInterface $current_user, EntityTypeManagerInterface $entityTypeManager) {
    $this->originalService = $original_service;
    $this->config = $config->get('simple_password_policy.settings');
    $this->policy = $policy;
    $this->currentUser = $entityTypeManager->getStorage('user')->load($current_user->id());
  }

  /**
   * This function is used to generate strong password.
   *
   * @param int|null $length
   *   The length of the password.
   *
   * @return string
   *   The strong password.
   */
  public function generate(int $length = NULL):string {

    // Force the policy length.
    $length = $this->config->get('min_length');

    // Generate a strong password.
    while (TRUE) {
      $password = '';

      // Generate a password of the policy length.
      for ($i = 0; $i < $length; $i++) {
        $password .= chr(random_int(33, 126));
      }

      $errors = $this->policy->validate($this->currentUser, $password);

      if (empty($errors)) {
        return $password;
      }
    }
  }

}
