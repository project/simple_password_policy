<?php
namespace Drupal\simple_password_policy\Event;

use Drupal\simple_password_policy\PasswordPolicyInterface;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a users password is about to expire.
 */
class PasswordPolicyWarningEvent extends Event {

  const EVENT_NAME = 'simple_password_policy_warning';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * The password policy.
   *
   * @var \Drupal\simple_password_policy\PasswordPolicyInterface
   */
  public $policy;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account of the user whos password is about to expire.
   *
   * @param \Drupal\simple_password_policy\PasswordPolicyInterface $policy
   *   The password policy that triggered the event.
   */
  public function __construct(UserInterface $account, PasswordPolicyInterface $policy) {
    $this->account = $account;
    $this->policy = $policy;
  }

}
