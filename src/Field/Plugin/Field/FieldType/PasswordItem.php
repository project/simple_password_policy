<?php
namespace Drupal\simple_password_policy\Field\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\PasswordItem as BasePasswordItem;
use Drupal\simple_password_policy\Exception\PasswordPolicyException;
use Drupal\user\UserInterface;

/**
 * Overrides the 'password' entity field type.
 */
class PasswordItem extends BasePasswordItem {

  /**
   * {@inheritdoc}
   */
  public function preSave() {

    // When a user entity is saved, the password value is hashed by the
    // field type preSave before entity->preSave hook is executed. So we can't
    // check if the password is valid in hook_enity_type_presave().

    /** @var UserInterface $user */
    $entity = $this->getEntity();

    // Match conditions to the parent preSave method where the password
    // is getting hashed.
    if (
      // If it is already hashed, we can't check it, let the parent handle that.
      !$this->pre_hashed
      // If it is empty, let the parent handle that.
      && !empty($this->value)
      // Skip if it's not a user we are checking (shouldn't happen ?? ).
      && $entity instanceof UserInterface
      // Match the condition of the parent where the hash is hapening.
      && ($entity->isNew() || (strlen(trim($this->value)) > 0 && $this->value != $entity->original->{$this->getFieldDefinition()->getName()}->value))
    ) {

      /** @var \Drupal\simple_password_policy\PasswordPolicy $password_policy */
      $password_policy = \Drupal::service("simple_password_policy");

      $errors = $password_policy->validate($entity, $this->value);
      if (!empty($errors)) {
        throw new PasswordPolicyException($password_policy, $entity, $errors);
      }
    }

    parent::preSave();
  }

}
