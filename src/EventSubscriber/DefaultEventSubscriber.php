<?php

namespace Drupal\simple_password_policy\EventSubscriber;

use Drupal\simple_password_policy\Event\PasswordPolicyExpireEvent;
use Drupal\simple_password_policy\Event\PasswordPolicyWarningEvent;
use Drupal\simple_password_policy\PasswordPolicyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Default password policy events handler.
 */
class DefaultEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * DefaultEventSubscriber constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Event callback to PasswordPolicyExpireEvent.
   *
   * @param \Drupal\simple_password_policy\Event\PasswordPolicyExpireEvent $event
   */
  public function passwordPolicyExpireEvent(PasswordPolicyExpireEvent $event) {

    $account = $event->account;
    $password_policy = $event->policy;

    $this->messenger->addError($this->getExpireMessage($account, $password_policy));

    $url = new Url('entity.user.edit_form', ['user' => $account->id()]);
    $url = $url->setAbsolute()->toString();

    $response = new RedirectResponse($url);
    $response->send();
  }

  /**
   * Event callback to AccountPolicyWarningEvent.
   *
   * @param \Drupal\simple_password_policy\Event\PasswordPolicyWarningEvent $event
   */
  public function passwordPolicyWarningEvent(PasswordPolicyWarningEvent $event) {

    $account = $event->account;
    $password_policy = $event->policy;

    // Always set a message for the user to remind him his password is about to expire soon.
    $this->messenger->addWarning($this->getExpireMessage($account, $password_policy));

    // Send out a warning email only once.
    if (!$password_policy->warningIssued($account)) {
      $params['account'] = $account;
      $langcode = $account->getPreferredLangcode();

      // Get the custom site notification email to use as the from email address
      // if it has been set.
      $site_mail = \Drupal::config('system.site')->get('mail_notification');
      // If the custom site notification email has not been set, we use the site
      // default for this.
      if (empty($site_mail)) {
        $site_mail = \Drupal::config('system.site')->get('mail');
      }
      if (empty($site_mail)) {
        $site_mail = ini_get('sendmail_from');
      }

      // Get the from email if set in config.
      $config = \Drupal::config('simple_password_policy.settings');
      if (!empty($config->get('expire_warning_mail.from'))) {
        $site_mail = $config->get('expire_warning_mail.from');
      }

      /** @var \Drupal\Core\Mail\MailManagerInterface $mail */
      $mail = \Drupal::service('plugin.manager.mail');
      $mail->mail('simple_password_policy', 'expire_warning_mail', $account->getEmail(), $langcode, $params, $site_mail);

      /** The actual message is handled in @see simple_password_policy_mail. */
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[PasswordPolicyWarningEvent::EVENT_NAME][] = ['passwordPolicyWarningEvent'];
    $events[PasswordPolicyExpireEvent::EVENT_NAME][] = ['passwordPolicyExpireEvent'];
    return $events;
  }

  /**
   * @inheritDoc
   */
  protected function getExpireMessage(UserInterface $user, PasswordPolicyInterface $passwordPolicy) {
    $expire_time = $passwordPolicy->getExpireTime($user);
    if (!empty($expire_time)) {
      return time_ago($expire_time,
        "Your password will expire @output.",
        "Your password has expired, please update it."
      );
    }
    else {
      return $this->t("Your password will not expire.");
    }
  }
}
