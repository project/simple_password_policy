<?php

namespace Drupal\simple_password_policy\EventSubscriber;

use Drupal\simple_password_policy\PasswordPolicyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\RouteObjectInterface;

/**
 * Checks each request if the pqssword has become expired.
 */
class RequestEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**s
   * The currently logged in user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * The password policy service.
   *
   * @var \Drupal\simple_password_policy\PasswordPolicyInterface $password_policy
   */
  protected $password_policy;

  /**
   * PasswordPolicyEventSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The currently logged in user.
   * @param \Drupal\simple_password_policy\PasswordPolicyInterface $passwordPolicy
   *   The password policy service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(AccountProxyInterface $currentUser, PasswordPolicyInterface $passwordPolicy, EntityTypeManagerInterface $entityTypeManager, RequestStack $requestStack) {
    $this->currentUser = $currentUser;
    $this->password_policy = $passwordPolicy;
    $this->request = $requestStack->getCurrentRequest();
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * Event callback to look for users expired password.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   */
  public function checkForUserPasswordExpiration(RequestEvent $event) {
    // There needs to be an explicit check for non-anonymous or else
    // this will be tripped and a forced redirect will occur.
    if ($this->currentUser->isAuthenticated()) {

      $is_ajax = $this->request->headers->get('X_REQUESTED_WITH') === 'XMLHttpRequest';
      if (!$is_ajax) {
        $route_name = $this->request->attributes->get(RouteObjectInterface::ROUTE_NAME);

        if (!$this->password_policy->shouldIgnoreRoute($route_name)) {

          /* @var $user \Drupal\user\UserInterface */
          $user = $this->userStorage->load($this->currentUser->id());

          if ($this->password_policy->applyPolicy($user)) {
            if ($this->password_policy->isExpired($user)) {
              $this->password_policy->expire($user);
            }
            else {
              if ($this->password_policy->shouldIssueWarning($user)) {
                $this->password_policy->issueWarning($user);
              }
            }
          }
        }
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['checkForUserPasswordExpiration'];
    return $events;
  }

}
