<?php

namespace Drupal\simple_password_policy\Exception;

use Drupal\Component\Plugin\Exception\ExceptionInterface;
use Drupal\simple_password_policy\PasswordPolicyInterface;
use Drupal\user\UserInterface;

/**
 * Class PasswordPolicyException.
 */
class PasswordPolicyException extends \Exception implements ExceptionInterface {

  /**
   * A password policy exception.
   *
   * @param \Drupal\simple_password_policy\PasswordPolicyInterface $policy
   *   The policy that was used to generate the errors.
   * @param \Drupal\user\UserInterface $user
   *   The user for which these errors occurred.
   * @param array $errors
   *   An array of policy errors as returned by PasswordPolicy::validate();
   */
  public function __construct(PasswordPolicyInterface $policy, UserInterface $user, array $errors = []) {
    parent::__construct($policy->policy($user, $errors));
  }

}
