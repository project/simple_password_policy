<?php
namespace Drupal\simple_password_policy;

use Drupal\user\UserInterface;

interface PasswordPolicyInterface {

  /**
   * Decide if the policy should be applied for this user or not.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return bool
   */
  public function applyPolicy(UserInterface $user);

  /**
   * Validate a password string for a given user.
   *
   * @param \Drupal\user\UserInterface $user
   * @param string $pass
   *
   * @return array
   */
  public function validate(UserInterface $user, $pass);

  /**
   * Return the password policy message for the given user.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   The user to which the policy should be applied.
   * @param array $errors
   *   Array containing the rules that are invalid.
   *
   * @return string
   */
  public function policy(UserInterface $user = null, array $errors = []);

  /**
   * Store a user's current password in the password table.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @throws \Exception
   */
  public function store(UserInterface $user);

  /**
   * Get the password expiration timestamp for a user.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return int
   */
  public function getExpireTime(UserInterface $user);

  /**
   * Get the password expiration warning timestamp for a user.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return int
   */
  public function getWarningTime(UserInterface $user);

    /**
   * Get a message stating when the current password will expire or is expired.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return string
   */
  public function getWarningPeriod(UserInterface $user);

  /**
   * Check if a route should be ignored
   *
   * @param string $route_name
   *
   * @return bool
   */
  public function shouldIgnoreRoute($route_name);

  /**
   * Decide if we need to issue a warning or not.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return bool
   */
  public function shouldIssueWarning(UserInterface $user);

  /**
   * Check if a user already got a warning.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return bool
   */
  public function warningIssued(UserInterface $user);

  /**
   * Send out a warning event.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return bool
   */
  public function issueWarning(UserInterface $user);

  /**
   * Check if a user his password has expired.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return bool
   */
  public function isExpired(UserInterface $user);

  /**
   * Expire a user.
   *
   * @param \Drupal\user\UserInterface $user
   *
   */
  public function expire(UserInterface $user);

}
