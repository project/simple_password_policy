<?php
namespace Drupal\simple_password_policy;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Password\PasswordInterface;
use Drupal\simple_password_policy\Event\PasswordPolicyExpireEvent;
use Drupal\simple_password_policy\Event\PasswordPolicyWarningEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PasswordPolicy implements PasswordPolicyInterface {

  use StringTranslationTrait;

  /** @var \Drupal\Core\Database\Connection $db */
  protected $db;

  /** @var StateInterface $state */
  protected $state;

  /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
  protected $event_dispatcher;

  /** @var \Drupal\Core\Password\PasswordInterface $passwordService */
  protected $passwordService;

  /** @var ConfigFactoryInterface $config_factory */
  protected $config_factory;

  /** @var \Drupal\Core\Render\RendererInterface $renderer */
  protected $renderer;

  // Policy validation parameters.
  protected $config;

  /**
   * PasswordPolicy constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   * @param \Drupal\Core\Password\PasswordInterface $passwordService
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $database, StateInterface $state, EventDispatcherInterface $eventDispatcher, PasswordInterface $passwordService, RendererInterface $renderer) {
    $this->db = $database;
    $this->state = $state;
    $this->event_dispatcher = $eventDispatcher;
    $this->renderer = $renderer;
    $this->passwordService = $passwordService;
    $this->config_factory = $config_factory;
    $this->config = $this->config_factory->getEditable('simple_password_policy.settings');
  }

  /**
   * @inheritDoc
   */
  public function applyPolicy(UserInterface $user) {

    $ignore_users = array_filter($this->config->get('ignore_users') ?? []);

    return $user
      // Don't apply if user has bypass permission
      && !$user->hasPermission("bypass password policy")
      // Don't apply if user need to be ignored (by account name)
      && !in_array($user->getAccountName(), $ignore_users)
      // Don't apply if user need to be ignored (by email)
      && !in_array($user->getEmail(), $ignore_users)
      ;
  }

  /**
   * @inheritDoc
   */
  public function validate(UserInterface $user, $pass) {
    $errors = [];

    if ($this->applyPolicy($user)) {

      // Minimum password length.
      $min_length = $this->config->get('min_length') ?? '';
      if (!empty($min_length) && strlen($pass) < $min_length) {
        $errors[] = 'min_length';
      }

      // Check if password is similar to username.
      $similar_username = $this->config->get('similar_username') ?? '';
      if (!empty($similar_username) || $similar_username === 0) {
        $similar_username = $similar_username === true ? 100 : $similar_username;
        similar_text($pass, $user->getAccountName(), $similar_pct);
        if ($similar_pct >= $similar_username) {
          $errors[] = 'similar_username';
        }
      }

      // Check minimum character type occurence rules.
      $min_to_check = [
        'min_lowercase' => '![^a-z]+!',
        'min_uppercase' => '![^A-Z]+!',
        'min_special' => '![a-zA-Z0-9]+!',
        'min_numeric' => '![^0-9]+!'
      ];

      foreach ($min_to_check as $config_key => $pattern) {
        $config_value = $this->config->get($config_key) ?? '';
        if (!empty($config_value) || $config_value === 0) {
          if ($config_value === 0) {
            // Count must zero.
            if (mb_strlen(preg_replace($pattern, '', $pass))) {
              $errors[] = $config_key;
            }
          }
          else {
            // Count must be higher than config value.
            if (mb_strlen(preg_replace($pattern, '', $pass)) < $config_value) {
              $errors[] = $config_key;
            }
          }
        }
      }

      // Min number of old passwords allowed within an optional timespan.
      $min_old = $this->config->get('min_old') ?? '';
      if (!empty($min_old) || $min_old === 0) {
        $entries = $this->getPasswords($user, $pass);

        $min_old_age = $this->config->get('min_old_age') ?? '';
        if (!empty($min_old_age) ||  $min_old_age === 0) {
          $threshold = time() - $min_old_age;
          $entries = array_filter($entries, function ($changed) use ($threshold) {
            return ($changed > $threshold);
          }, ARRAY_FILTER_USE_KEY);
        }

        if ($min_old < count($entries)) {
          $errors[] = 'min_old';
        }

      }
    }

    return $errors;
  }

  /**
   * @inheritDoc
   */
  public function policy(UserInterface $user = null, array $errors = []) {

    if ($this->applyPolicy($user)) {
      $policy = [];

      $min_length = $this->config->get('min_length') ?? '';
      if (!empty($min_length) && $min_length > 0) {
        $policy[] = [
          '#wrapper_attributes' => ['class' => [in_array('min_length', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
          '#markup' => (string) $this->formatPlural($min_length, "Must be at least 1 character long.", "Must be at least @count characters long.")
        ];
      }

      // Check if password is similar to username.
      $similar_username = $this->config->get('similar_username') ?? '';
      if (!empty($similar_username) ||  ((int) $similar_username > 0 && (int) $similar_username < 100)) {
        $policy[] = [
          '#wrapper_attributes' => ['class' => [in_array('similar_username', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
          '#markup' => (string) $this->t("The password cannot be similar to the username.")
        ];
      }

      $min_lowercase = $this->config->get('min_lowercase') ?? '';
      if (!empty($min_lowercase) ||  $min_lowercase === 0) {
        if ($min_lowercase === 0) {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_lowercase', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->t("May not contain a lowercase character.")
          ];
        }
        else {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_lowercase', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->formatPlural($min_lowercase, "Must contain at least 1 lowercase character.", "Must contain at least @count lowercase characters.")
          ];
        }
      }

      $min_uppercase = $this->config->get('min_uppercase') ?? '';
      if (!empty($min_uppercase) || $min_uppercase === 0) {
        if ($min_uppercase === 0) {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_lowercase', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->t("May not contain an uppercase character.")
          ];
        }
        else {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_uppercase', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->formatPlural($min_uppercase, "Must contain at least 1 uppercase character.", "Must contain at least @count uppercase characters.")
          ];
        }
      }

      $min_special = $this->config->get('min_special') ?? '';
      if (!empty($min_special) || $min_special === 0) {
        if ($min_special === 0) {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_lowercase', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->t("May not contain a special character.")
          ];
        }
        else {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_special', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->formatPlural($min_special, "Must contain at least 1 special character.", "Must contain at least @count special characters.")
          ];
        }
      }

      $min_numeric = $this->config->get('min_numeric') ?? '';
      if (!empty($min_numeric) ||  $min_numeric === 0) {
        if ($min_numeric === 0) {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_lowercase', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->t("May not contain a numeric character.")
          ];
        }
        else {
          $policy[] = [
            '#wrapper_attributes' => ['class' => [in_array('min_numeric', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
            '#markup' => (string) $this->formatPlural($min_numeric, "Must contain at least 1 numeric character.", "Must contain at least @count numeric characters")
          ];
        }
      }

      $min_old = $this->config->get('min_old') ?? '';
      $min_old_age = $this->config->get('min_old_age') ?? '';
      if (!empty($min_old) || $min_old === 0) {
        if (!empty($min_old_age) && $min_old_age > 0) {
          $period = floor($min_old_age / (24 * 3600));
          if ($min_old == 0) {
            $min_old_rule = (string) $this->t("The password cannot be re-used in the past @period days.", [
              '@period' => $period
            ]);
          }
          else {
            $min_old_rule = (string) $this->formatPlural($min_old, "The password cannot be re-used only 1 time in the past @period days.", "The password cannot be re-used only @count times in the past @period days.", [
              '@period' => $period
            ]);
          }
        }
        else {
          if ($min_old == 0) {
            $min_old_rule = (string) $this->t("The password cannot be re-used.");
          }
          else {
            $min_old_rule = (string) $this->formatPlural($min_old, "The password cannot be re-used only 1 time.", "The password cannot be re-used only @count times.");
          }
        }

        $policy[] = [
          '#wrapper_attributes' => ['class' => [in_array('min_old', $errors) ? 'password-policy-invalid-rule marker' : 'password-policy-valid-rule']],
          '#markup' => $min_old_rule
        ];
      }

      if (!empty($policy)) {
        $policy = [
          'title' => [
            '#markup' => (string) $this->formatPlural(count($policy), "The password must satisfy the following password policy rule:", "The password must satisfy the following password policy rules:"),
          ],
          'rules' => [
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#items' => $policy
          ]
        ];

        $message = $this->renderer->renderPlain($policy);
      }
      else {
        // There is no policy.
        $message = "";
      }
    }
    else {
      $message = $this->t("This user bypasses the password policy.");
    }

    if (PHP_SAPI === 'cli') {
      $message = PlainTextOutput::renderFromHtml($message);
    }

    return $message;
  }

  /**
   * @inheritDoc
   */
  public function store(UserInterface $user) {
    $this->db->insert("user_pass")->fields([
      'uid' => $user->id(),
      'changed' => \Drupal::time()->getRequestTime(),
      'pass' => $user->getPassword(),
    ])->execute();
  }

  /**
   * @inheritDoc
   */
  protected function getIgnoreRoutes() {
    $ignore_routes = $this->config->get('ignore_routes') ?? [];

    return array_unique(array_merge($ignore_routes, [
      'entity.user.edit_form',
      'system.ajax',
      'user.logout',
      'admin_toolbar_tools.flush',
      'user.pass',
    ]));

  }

  /**
   * @inheritDoc
   */
  protected function getPasswords(UserInterface $user, $pass = "") {

    $query = $this->db->select("user_pass", 'up');
    $query->fields('up');
    $query->condition('up.uid', $user->id());
    $query->orderBy('up.changed', 'DESC');

    $passwords = $query->execute()->fetchAllAssoc('changed', \PDO::FETCH_ASSOC);

    if (!empty($pass)) {
      $passwords = array_filter($passwords, function ($value) use ($pass) {
        return $this->passwordService->check($pass, $value['pass']);
      });
    }

    return $passwords;
  }

  /**
   * Get the last time the password was changed.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return int
   */
  protected function getLastPasswordChanged(UserInterface $user) {

    // Fetch the last password matching the current user password.
    $query = $this->db->select("user_pass", 'up');
    $query->fields('up');
    $query->condition('up.uid', $user->id());
    $query->condition('up.pass', $user->getPassword());
    $query->orderBy('up.changed', 'DESC');
    $query->range(0, 1);

    $last_password = $query->execute()->fetchAssoc();

    if (!$last_password) {
      // Fallback to the last time the user profile changed.
      return $user->getChangedTime();
    }

    return intval($last_password['changed']);
  }

  /**
   * @inheritDoc
   */
  public function getExpireTime(UserInterface $user) {

    $expire_time = 0;

    $expire_period = (string) $this->config->get('expire_period') ?? '';

    if (!empty($expire_period)) {

      $lastPasswordChanged = $this->getLastPasswordChanged($user);
      if ($lastPasswordChanged === '0') {
        $lastPasswordChanged = $user->getCreatedTime();
      }

      if (ctype_digit($expire_period)) {
        $expire_time = $lastPasswordChanged + intval($expire_period);
      }
      else {
        $expire_time = time() + ($lastPasswordChanged - strtotime('-' . $expire_period));
      }
    }

    return $expire_time;
  }

  /**
   * @inerhitDoc
   */
  public function getWarningTime(UserInterface $user) {
    $warning_time = 0;

    $expire_time = $this->getExpireTime($user);

    if ($expire_time) {

      $expire_warning = (string) $this->config->get('expire_warning') ?? '';

      if (!empty($expire_warning)) {

        if (ctype_digit($expire_warning)) {
          $warning_time = $expire_time - intval($expire_warning);
        }
        else {
          $warning_time = time() + ($expire_time - strtotime('+' . $expire_warning));
        }

      }
    }

    return $warning_time;
  }

  /**
   * @inerhitDoc
   */
  public function getWarningPeriod(UserInterface $user) {
    $warning_time = $this->getWarningTime($user);
    if ($warning_time) {
      return time_ago($warning_time);
    }
    else {
      return "";
    }
  }

  /**
   * @inerhitDoc
   */
  public function shouldIgnoreRoute($route_name) {
    return in_array($route_name, $this->getIgnoreRoutes());
  }

  /**
   * @inheritDoc
   */
  public function shouldIssueWarning(UserInterface $user) {
    $warning_time = $this->getWarningTime($user);
    return ( $warning_time && (time() > $warning_time));
  }

  /**
   * @inheritDoc
   */
  public function warningIssued(UserInterface $user) {
    $warned_users = $this->state->get('simple_password_policy.warned_users', []);
    return key_exists($user->id(), $warned_users);
  }

  /**
   * @inheritDoc
   */
  public function issueWarning(UserInterface $user) {
    // Keep track of issued warnings.
    $warned_users = $this->state->get('simple_password_policy.warned_users', []);
    $warned_users[$user->id()] = $user->id();
    $this->state->set('simple_password_policy.warned_users', $warned_users);

    // Send out an event so others can react to this as well.
    $event = new PasswordPolicyWarningEvent($user, $this);
    $this->event_dispatcher->dispatch($event, PasswordPolicyWarningEvent::EVENT_NAME);
  }

  /**
   * @inerhitDoc
   */
  public function isExpired(UserInterface $user) {
    $expire_time = $this->getExpireTime($user);
    return ($expire_time && (time() > $expire_time));
  }

  /**
   * @inerhitDoc
   */
  public function expire(UserInterface $user) {
    // Send out an event so others can react to this as well.
    $event = new PasswordPolicyExpireEvent($user, $this);
    $this->event_dispatcher->dispatch($event, PasswordPolicyExpireEvent::EVENT_NAME);
  }

}
