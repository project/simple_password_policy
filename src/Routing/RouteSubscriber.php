<?php

namespace Drupal\simple_password_policy\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events and restrict access to user.pass route.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $pass_reset_disabled = \Drupal::config('simple_password_policy.settings')->get('disable_password_reset');
    if ($route = $collection->get('user.pass')) {
      if ($pass_reset_disabled) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
