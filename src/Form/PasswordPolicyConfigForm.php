<?php

namespace Drupal\simple_password_policy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements custom config form to configure the account policy.
 */
class PasswordPolicyConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'password_policy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_password_policy.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('simple_password_policy.settings');

    $form['#title'] = $this->t('Password policy settings');

    $form['policy_rules'] = [
      '#type' => 'details',
      '#title' => $this->t('Policy rules'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
    ];

    $form['policy_rules']['min_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum length'),
      '#description' => $this->t("Minimum number of characters for a password. Leave empty to skip this check."),
      '#default_value' => $config->get('min_length') ?? 12,
    ];

    $form['policy_rules']['min_lowercase'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lowercase characters'),
      '#description' => $this->t("Minimum number of lowercase characters. Leave empty to skip this check."),
      '#default_value' => $config->get('min_lowercase') ?? 1,
    ];

    $form['policy_rules']['min_uppercase'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Uppercase characters'),
      '#description' => $this->t("Minimum number of uppercase characters. Leave empty to skip this check."),
      '#default_value' => $config->get('min_uppercase') ?? 1,
    ];

    $form['policy_rules']['min_numeric'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Numeric characters'),
      '#description' => $this->t("Minimum number of numeric characters. Leave empty to skip this check."),
      '#default_value' => $config->get('min_numeric') ?? 1,
    ];

    $form['policy_rules']['min_special'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Special characters'),
      '#description' => $this->t("Minimum number of special characters. Leave empty to skip this check."),
      '#default_value' => $config->get('min_special') ?? 1,
    ];

    $form['policy_rules']['min_old'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Old passwords'),
      '#description' => $this->t("Minimum number of previous old passwords that can be re-used. Leave empty to skip this check."),
      '#default_value' => $config->get('min_old') ?? 0,
    ];

    $form['policy_rules']['min_old_age'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Old passwords period'),
      '#description' => $this->t("If old passwords are allowed, enter a time span in seconds (for example only allow passwords from the last year). Leave empty to skip this check."),
      '#default_value' => $config->get('min_old_age') ?? '',
    ];

    $form['policy_rules']['similar_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Similar to username check'),
      '#description' => $this->t("Give an upper similarity threshold (percentage 0-100) of how much the password and the username are allowed to be similar. 0 means the password can not be the same as the username, 100 means it can be the same. Leave empty to skip this check."),
      '#default_value' => $config->get('similar_username') ?? 100,
    ];

    $form['policy_bypass'] = [
      '#type' => 'details',
      '#title' => $this->t('Policy bypass'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
    ];

    $form['policy_bypass']['ignore_routes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ignore routes'),
      '#description' => $this->t("Add a route name on each line to exclude password policy checks on."),
      '#default_value' => implode("\n", $config->get('ignore_routes') ?? []),
    ];

    $form['policy_bypass']['ignore_users'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Username ignore patterns'),
      '#description' => $this->t("Add a username pattern on each line to exclude specific users from the policy by their username. You can use regex notation. You can also exlcude users from the policy based on roles by giving a role the 'bypass password policy' permission."),
      '#default_value' => implode("\n", $config->get('ignore_users') ?? []),
    ];

    $form['expire_scheduling'] = [
      '#type' => 'details',
      '#title' => $this->t('Scheduling'),
      '#description' => $this->t('Set timings on when a users password must be automatically expire.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
    ];

    $form['expire_scheduling']['expire_period'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expire period'),
      '#description' => $this->t("Let passwords expire after a specific period. Use seconds or a strtotime phrase (eg 3 months). Leave empty to skip this check."),
      '#default_value' => $config->get('expire_period') ?? '1 year',
    ];

    $form['expire_scheduling']['expire_warning'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expire warning'),
      '#description' => $this->t("When passwords expire, issue a warning in advance. Use seconds or a strtotime phrase (eg 3 weeks). Leave empty to skip this."),
      '#default_value' => $config->get('expire_warning') ?? '3 weeks',
    ];

    $form['expire_warning_mail'] = [
      '#type' => 'details',
      '#title' => $this->t('Warning email'),
      '#description' => $this->t('Email send out when the inactive warning period has expired.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
    ];

    $form['expire_warning_mail']['expire_warning_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('expire_warning_mail.subject'),
      '#maxlength' => 180,
    ];
    $form['expire_warning_mail']['expire_warning_mail_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('expire_warning_mail.body'),
      '#rows' => 8,
    ];
    $form['disable_password_reset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Password Reset'),
      '#default_value' => $config->get('disable_password_reset'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->configFactory()->getEditable('simple_password_policy.settings');

    $config->set('min_length', $form_state->getValue('min_length'))
      ->set('min_lowercase', $form_state->getValue('min_lowercase'))
      ->set('min_uppercase', $form_state->getValue('min_uppercase'))
      ->set('min_numeric', $form_state->getValue('min_numeric'))
      ->set('min_special', $form_state->getValue('min_special'))
      ->set('min_old', $form_state->getValue('min_old'))
      ->set('min_old_age', $form_state->getValue('min_old_age'))
      ->set('similar_username', $form_state->getValue('similar_username'))
      ->set('ignore_routes', explode('\n', $form_state->getValue('ignore_routes')))
      ->set('ignore_users', explode('\n', $form_state->getValue('ignore_users')))
      ->set('expire_period', $form_state->getValue('expire_period'))
      ->set('expire_warning', $form_state->getValue('expire_warning'))
      ->set('expire_warning_mail.body', $form_state->getValue('expire_warning_mail_body'))
      ->set('expire_warning_mail.subject', $form_state->getValue('expire_warning_mail_subject'))
      ->set('disable_password_reset', $form_state->getValue('disable_password_reset'))
      ->save();
  }

}
