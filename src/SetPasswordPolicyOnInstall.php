<?php

namespace Drupal\simple_password_policy;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Batch\BatchBuilder;

/**
 * Class SetPasswordPolicyOnInstall.
 *
 * Initialize users with Simple Password Policy during installation.
 */
class SetPasswordPolicyOnInstall {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * SetPasswordPolicyOnInstall constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
  }

  /**
   * Start the batch process to initialize users with Simple Password Policy.
   */
  public function initializeUsersBatch() {
    // Set up the batch process.
    $batch_builder = new BatchBuilder();
    $batch_builder
      ->setTitle($this->t('Initializing users with Simple Password Policy'))
      ->setFinishCallback([$this, 'passwordPolicyOnInstallBatchProcessCallback']);

    // Add the batch operation to initialize users.
    $batch_builder->addOperation([$this, 'initializeUsersBatchProcess'], []);

    // Start the batch process.
    batch_set($batch_builder->toArray());
  }

  /**
   * Process a batch of users to initialize with Simple Password Policy.
   *
   * @param array $context
   *   The batch context.
   */
  public function initializeUsersBatchProcess(&$context) {
    // Initialize batch context if not already set.
    if (!isset($context['sandbox']['total'])) {
      // Get the query for all user IDs.
      $query = $this->entityTypeManager->getStorage('user')->getQuery();
      $query->condition('uid', 0, '>');
      $query->accessCheck(FALSE); // Disable access check
      $context['sandbox']['uids'] = $query->execute();
      // Set total users and current progress to zero.
      $context['sandbox']['total'] = count($context['sandbox']['uids']);
      $context['sandbox']['current'] = 0;
      $context['results'] = [];
    }

    // Define batch size.
    $batch_size = 100;
    // Get a chunk of user IDs to process.
    $uids_chunk = array_slice($context['sandbox']['uids'], $context['sandbox']['current'], $batch_size);

    // Process each user in the chunk.
    foreach ($uids_chunk as $uid) {
      $this->initializeUser($uid, $context);
      $context['sandbox']['current']++;
    }

    // Update progress message.
    $context['message'] = $this->t('Processed @current out of @total users.', [
      '@current' => $context['sandbox']['current'],
      '@total' => $context['sandbox']['total'],
    ]);

    // Calculate and set batch progress.
    $context['finished'] = $context['sandbox']['current'] / $context['sandbox']['total'];
  }

  /**
   * Initialize a user with Simple Password Policy.
   *
   * @param int $uid
   *   The user ID.
   * @param array $context
   *   The batch context.
   */
  public function initializeUser($uid, &$context) {
    // Load the user entity.
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    // Check if the loaded entity is a user.
    if ($user instanceof AccountInterface) {
      // Initialize the user with Simple Password Policy.
      $password_policy = \Drupal::service('simple_password_policy');
      $password_policy->store($user);
      // Add the processed user ID to the results.
      $context['results'][] = $uid;
    }
  }

  /**
   * Batch process callback function.
   *
   * @param bool $success
   *   Whether the batch process was successful.
   * @param array $results
   *   The results of the batch process.
   * @param array $operations
   *   The batch operations.
   */
  public function passwordPolicyOnInstallBatchProcessCallback($success, $results, $operations) {
    // Display messages based on batch process success.
    if ($success) {
      $count = count($results);
      $this->messenger->addMessage($this->t('@count Number of users have been initialized with Simple Password Policy.', ['@count' => $count]));
    }
    else {
      $this->messenger->addMessage($this->t('Finished with an error.'));
    }
  }

}
