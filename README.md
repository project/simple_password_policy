# Simple password policy

This module implements a simple account policy with the following
configurable set of fixed rules:
- minimum password length
- minimum amount of lowercase chars (a-z)
- minimum amount of uppercase chars (A-Z)
- minimum amount of numeric chars (0-9)
- minimum amount of special chars not(a-z A-Z 0-9)
- minimum amount of old password allowed
- period in which old passwords are not allowed
- check password is not similar to username

The module will also notify the user his password is expired and will
send a warning mail ahead of time.
- expire the password after period
- send email password is about to expire

Password policy checks can be ignored using:
- ignore password policy expiration check on configured routes:
- don't apply policy for users matching a pattern
- applying the permission 'bypass password policy'.

NOTE: Roles with a 'bypass password policy' permission, means the password policy
for users having such a role is not applied.
It does not mean users having such a role, can create other users with a
non-policy password! If you want to create a user with a non-policy password, you
have to assign him a role with this permission (either during user creation or afterwards).

You can also disable reset password links.

Drush upwd (user:password) will check the policy.

Overrides the core password_generator service to generate policy compliant passwords.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/simple_password_policy).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/simple_password_policy).


## Similar modules

If you want more control over the rules and how a password policy should
apply, the password_policy module is more
enhanced. If you want more strict rules, password_strength might be an
option too. This module was build to keep it
simple.

- password_policy
- password_strength


## Requirements

This does not require any other module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

**General usage**

After installing the module is configured with these default rules:
1. min_length: 12
2. min_lowercase: 1
3. min_uppercase: 1
4. min_numeric: 1
5. min_special: 1
6. min_old: ''
7. min_old_age: ''
8. similar_username: ''
9. ignore_routes:
    1. 'entity.user.edit_form'
    2. 'system.ajax'
    3. 'user.logout'
    4. 'admin_toolbar_tools.flush'
    5. 'user.pass'
    6. 'image.style_public'
10.  ignore_users: { }
11.  expire_period: '1 year'
12.  expire_warning: '3 weeks'

Configuration is found under the "`People`" configuration item.
`/admin/config/people/password_policy`


## Maintainers

- Mschudders - [Mschudders](https://www.drupal.org/u/mschudders)
- Kris Booghmans - [kriboogh](https://www.drupal.org/u/kriboogh)

**This project has been sponsored by:**
- [Calibrate](https://www.calibrate.be)
  In the past fifteen years, we have gained large expertise in
  consulting organizations in various industries.
