(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.simple_password_policy_validatepolicy = {
    attach: function (context, settings) {
      var typingTimer;
      var delay = 500;
      $(once('validate_policy', '#edit-pass-pass1')).on('keyup', function (e) {
        clearTimeout(typingTimer);
        var trigid = $(this);
        typingTimer = setTimeout(function () {
          trigid.triggerHandler('validate_policy');
        }, delay);
      });
    }
  };
})(jQuery, Drupal);
